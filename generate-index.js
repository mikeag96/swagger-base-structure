const glob = require("glob");
const fs = require("fs/promises");

const mainPath = "api-docs";
const paths = [
  `${mainPath}/examples`,
  `${mainPath}/links`,
  `${mainPath}/parameters`,
  `${mainPath}/schemas`,
  `${mainPath}/responses`,
  `${mainPath}/security`,
];

const files = [];

const getFiles = (path) =>
  new Promise((resolve, reject) => {
    return glob(`${path}/**/*.yaml`, null, (er, files) => {
      if (er) {
        return reject(er);
      }

      return resolve(files.filter((e) => !e.includes("_index")));
    });
  });

const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const defineFile = async (paths) => {
  let file = `##############################################################################
# DO NOT MODIFY THIS FILE, IT IS A FILE THAT IS RECREATED IN EVERY COMPILATION
##############################################################################`;
  let mainPath = "";

  for (const path of paths) {
    const segments = path.split("/") || [];

    const fileName = segments[segments.length - 1];
    mainPath = `${segments[0]}/${segments[1]}`;

    const arrRestPath = segments.splice(2, segments.length - 3);
    let restPath = "";
    let operationName = "";

    for (let item of arrRestPath) {
      restPath += `${item}/`;

      item = item.replace(/\//g, " ").replace(/-/g, " ").replace(/_/g, " ");
      item = item.split(" ");

      for (const e of item) {
        operationName += `${capitalizeFirstLetter(e)}`;
      }
    }

    file += `
${operationName.trim()}${fileName.replace(".yaml", "").trim()}:
  $ref: "./${restPath.toLocaleLowerCase().trim()}${fileName.trim()}"
`;
  }

  file +=`
`

  await fs.writeFile(`./${mainPath}/_index.yaml`, file);
};

const generate = async () => {
  for (const path of paths) {
    files.push(await getFiles(path));
  }

  for (const items of files) {
    defineFile(items);
  }
};

generate();
