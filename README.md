<h1 align="center">Welcome to API Rest Documentation 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D8.1.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D16.13.0-blue.svg" />
  <a href="https://gitlab.com/mikeag96/swagger-base-structure" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://spdx.org/licenses/CC-BY-4.0.html" target="_blank">
    <img alt="License: CC--BY--4.0" src="https://img.shields.io/badge/License-CC--BY--4.0-yellow.svg" />
  </a>
</p>

> API Rest documentation

### 🏠 [Homepage](https://gitlab.com/mikeag96/swagger-base-structure)

## Prerequisites

- npm >=8.1.0
- node >=16.13.0
- docker

## Install

```sh
npm ci
```

## Usage

You can preview in real time your changes, you need run

```sh
npm run start
```

And access to [http://localhost:3000](http://localhost:3000)

You can edit openapi with Swagger Editor, you need run:

```sh
npm run build && docker compose up
```

And access to  [http://localhost:4567](http://localhost:4567)

## Author

👤 **Author Inc**

* Website: https://author.com/
* LinkedIn: [Author](https://www.linkedin.com/company/author/)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/mikeag96/swagger-base-structure/-/issues). You can also take a look at the [contributing guide](git@gitlab.com:mikeag96/swagger-base-structure/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

This project is [CC--BY--4.0](https://spdx.org/licenses/CC-BY-4.0.html) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_